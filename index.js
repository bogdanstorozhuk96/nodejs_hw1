const http = require("http");
const fs = require("fs");
const url = require("url");

const writeToLogs = (message) => {
  fs.readFile("./logs.json", (err, data) => {
    if(err){
      console.log(err.message); 
      return;
    }
    data = JSON.parse(data);
    data.logs.push({ message, time: +new Date() });
    fs.writeFile(`./logs.json`, JSON.stringify(data), (err) => {
      if(err){
        console.log(err.message);
        return;
      }
    });
  });
};

module.exports = () => {
  http
    .createServer((request, response) => {
      let {
        pathname,
        query: { filename, content, to, from },
      } = url.parse(request.url, true);
      if (request.method === "POST") {
        if (pathname.split("/")[1] === "file") {
          if (filename && content) {
            fs.writeFile(`./file/${filename}`, content, (err) => {
              if(err){
                console.log(err.message);
                return;
              }
              writeToLogs(`New file '${filename}.txt' added`);
              response.writeHead(200, { "Content-type": "text/html" });
              response.end("File was successfully created");
            });
          } else {
            response.writeHead(400, { "Content-type": "text/html" });
            response.end("Unrecognized query params");
          }
        } else {
          response.writeHead(404, { "Content-type": "text/html" });
          response.end(`Not found`);
        }
      } else if (request.method === "GET") {
        let type = pathname.split("/")[1];
        if (type === "file") {
          fs.readFile(`.${pathname}`, (err, content) => {
            if (err) {
              response.writeHead(400, { "Content-type": "text/html" });
              response.end(`No files with name "${pathname}" found`);
              return;
            }
            writeToLogs(`file '${pathname.slice(6)}' was read`);
            response.writeHead(200, { "Content-type": "text/html" });
            response.end(content);
          });
        } else if (type === "logs") {
          fs.readFile("./logs.json", (err, data) => {
            if(err){
              console.log(err.message);
              return;
            }
            data = JSON.parse(data);
            if (to && from) {
              to = Number(to);
              from = Number(from);
              data.logs = data.logs.filter((item) => {
                if (from <= item.time && item.time <= to) {
                  return true;
                }
              });
            }
            response.writeHead(200, { "Content-type": "application/json" });
            response.end(JSON.stringify(data));
          });
        } else {
          response.writeHead(404, { "Content-type": "text/html" });
          response.end(`Not found`);
        }
      }
    })
    .listen(Number(process.env.PORT) || 8080);
};
